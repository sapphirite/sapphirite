﻿using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Components;

namespace Sapphirite.Frontend.Client
{
    public enum PrerenderSide
    {
        Server,
        Client,
    }

    public interface IPrerenderState
    {
        PrerenderSide Side { get; }

        string StoredJson { get; }

        Task<T> LoadOrStore<T>(Func<T> factory);
    }

    public class PrerenderState : IPrerenderState
    {
        private IJSRuntime _jsRuntime;
        private bool _hasState;

        public string StoredJson { get; set; }

        public PrerenderSide Side { get; set; }

        public PrerenderState(IServiceProvider provider, PrerenderSide side)
        {
            _jsRuntime = provider.GetService<IJSRuntime>();

            Side = side;

            // Assume that if we're on the client we have stored state
            _hasState = side == PrerenderSide.Client;
        }

        public async Task<T> LoadOrStore<T>(Func<T> factory)
        {
            if (_hasState)
            {
                _hasState = false;

                // Retrieve the data and make sure it has anything
                var json = await _jsRuntime.InvokeAsync<string>("sapphiriteJsFunctions.getPrerenderData");

                if (!string.IsNullOrWhiteSpace(json))
                {
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }

            var data = factory();

            // Only store on the server
            if (Side == PrerenderSide.Server)
            {
                // This will be picked up later during pre-rendering the final page
                StoredJson = JsonConvert.SerializeObject(data);
            }

            return data;
        }
    }
}
